# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=grafana
pkgver=7.5.1
pkgrel=0
_commit=4dffb875cf # git rev-parse --short HEAD
_stamp=1616764341 # git --no-pager show -s --format=%ct
pkgdesc="Open source, feature rich metrics dashboard and graph editor"
url="https://grafana.com"
arch="all"
license="Apache-2.0"
makedepends="go"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/grafana/grafana/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz
	$pkgname.initd
	$pkgname.confd"

export GOPATH=$srcdir/go
export GOCACHE=$srcdir/go-build
export GOTMPDIR=$srcdir

# secfixes:
#   7.4.5-r0:
#     - CVE-2021-28146
#     - CVE-2021-28147
#     - CVE-2021-28148
#     - CVE-2021-27962
#   7.0.2-r0:
#     - CVE-2020-13379
#   6.3.4-r0:
#     - CVE-2019-15043

build() {
	local ldflags="-X main.version=$pkgver -X main.commit=$_commit -X main.buildstamp=$_stamp"
	go build -modcacherw -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-server
	go build -modcacherw -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-cli

}

check() {
	local pkgs="./..."

	case "$CARCH" in
	# https://github.com/grafana/grafana/issues/26389
	x86) pkgs="$(go list -modcacherw ./... | grep -Ev '(pkg/tsdb$)')" ;;
	# https://github.com/grafana/grafana/issues/26390
	s390x|mips64) pkgs="$(go list -modcacherw ./... | grep -Ev '(pkg/tsdb/influxdb/flux$)')" ;;
	esac

	# Grafana creates html tree in /tmp during test.
	# However instead of copying files it instead tries to hardlink them.
	# It then overwrites {index,error}.html with {index,error}-template.html
	# This is not a problem for most systems because /tmp is usually on tmpfs
	# and hardlinking there from another filesystem is not possible.
	# Also grafana official builder builds backend, then runs tests,
	# then builds frontend and finally runs frontend tests, so it's not a problem for them as well.
	# As this apkbuild uses prebuilt html assets (because upstream doesn't support building
	# on anything other than linux/amd64),
	# changing build order is much harder than simply backing up overwritten files
	# and restoring them after running tests.
	cp public/views/index.html public/views/index.bkp
	cp public/views/error.html public/views/error.bkp

	go test -modcacherw $pkgs

	mv public/views/index.bkp public/views/index.html
	mv public/views/error.bkp public/views/error.html
}

package() {
	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
	install -Dm755 "$builddir/$pkgname-server" "$pkgdir/usr/sbin/$pkgname-server"
	install -Dm755 "$builddir/$pkgname-cli" "$pkgdir/usr/bin/$pkgname-cli"
	install -Dm644 "$builddir/conf/sample.ini" "$pkgdir/etc/grafana.ini"
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/conf" "$builddir/public" "$pkgdir/usr/share/$pkgname/"
}

sha512sums="d2a1a560b6ad2472983eba925f3486a170c3b56068e030130d25fb05ddc97ab9b2a1ed583b2121b8516db51fbc254af30ac5f814cd3512fc7930a3b0d9712df6  grafana-7.5.1.tar.gz
bee8b95820a8d8983aa430ee1e7de65da0b90ecfe0eeab8d8a5486af818a4bede73846bcc5aae2c9735204be004171ae98bccd7546d3c0220030260f1f1ed203  grafana-7.5.1-bin.tar.gz
b0a781e1b1e33741a97e231c761b1200239c6f1235ffbe82311fe883387eb23bef262ad68256ebd6cf87d74298041b53b947ea7a493cfa5aa814b2a1c5181e13  grafana.initd
c2d9896ae9a9425f759a47aeab42b7c43b63328e82670d50185de8c08cda7b8df264c8b105c5c3138b90dd46e86598b16826457eb3b2979a899b3a508cbe4e8c  grafana.confd"
