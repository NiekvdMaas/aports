# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=pipewire
pkgver=0.3.24
pkgrel=0
pkgdesc="Multimedia processing graphs"
url="https://pipewire.org/"
arch="all !s390x !mips64" # unit tests fail on big-endian
license="LGPL-2.1-or-later"
makedepends="meson alsa-lib-dev libx11-dev sdl2-dev ffmpeg-dev eudev-dev dbus-dev
	glib-dev gstreamer-dev gst-plugins-base-dev sbc-dev doxygen xmltoman graphviz
	bluez-dev jack-dev vulkan-loader-dev pulseaudio-dev libopenaptx-dev"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-pulse
	$pkgname-jack
	$pkgname-lang
	"
source="https://gitlab.freedesktop.org/PipeWire/pipewire/-/archive/$pkgver/pipewire-$pkgver.tar.gz
	pipewire.desktop
	"

build() {
	abuild-meson \
		-Ddocs=enabled \
		-Dman=enabled \
		-Dgstreamer=enabled \
		-Dexamples=enabled \
		-Dffmpeg=enabled \
		-Dsystemd=disabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -Dm644 "$srcdir"/pipewire.desktop -t "$pkgdir"/etc/xdg/autostart/
}

pulse() {
	pkgdesc="Pulseaudio support for pipewire"
	install="$subpkgname.post-install $subpkgname.post-upgrade"
	provides="pulseaudio"
	provider_priority=1

	amove usr/bin/pipewire-pulse
	amove usr/lib/pipewire-0.3/libpipewire-module-protocol-pulse.so
	amove etc/pipewire/media-session.d/with-pulseaudio
}

jack() {
	pkgdesc="JACK support for pipewire"
	amove usr/lib/pipewire-*/jack
	amove usr/bin/pw-jack
	amove usr/lib/spa-*/jack/libspa-jack.so
	amove etc/pipewire/media-session.d/with-jack
}

sha512sums="be1fd3b15aae4fc276dd7e4be385cd58e6e9626d6e0a42b7bc3eb46ba324759c05320547aa857510ecf24b1628b76555222d342350d6406ad4d7b536cb3db497  pipewire-0.3.24.tar.gz
3977d6740108b4a521dab23e2f4ebe53a29e6663715fcc6b61d187265265fe65af157808e253a21cc61a116b26acfcd17e2f9b910c2715285154cfe51ef0877b  pipewire.desktop"
