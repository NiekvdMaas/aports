# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Newbyte <newbytee@protonmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pure-maps
pkgver=2.6.5
pkgrel=0
_commit_geomag="8eb9a730c8643fb7d63fdee4fd9a195ee8ba4df2"
pkgdesc="Maps and navigation"
url="https://github.com/rinigus/pure-maps"
# armhf blocked by mapbox-gl-qml -> qt5-qtdeclarative-dev
# s390x, mips64 blocked by mimic1
arch="all !armhf !s390x !mips !mips64"
license="GPL-3.0-or-later"
depends="
	geoclue
	kirigami2
	mapbox-gl-qml
	mimic1
	nemo-qml-plugin-dbus
	osmscout-server
	py3-gpxpy
	py3-pyotherside
	qml-module-clipboard
	qmlrunner
	qt5-qtbase-sqlite
	qt5-qtlocation
	qt5-qtmultimedia
	qt5-qtsensors
	"
makedepends="
	gettext
	py3-pyflakes
	python3
	qt5-qtbase-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qttools-dev
	qtchooser
	s2geometry-dev
	"
checkdepends="py3-pytest"
subpackages="$pkgname-lang"
source="https://github.com/rinigus/pure-maps/archive/$pkgver/pure-maps-$pkgver.tar.gz
	https://github.com/rinigus/geomag/archive/$_commit_geomag/geomag-$_commit_geomag.tar.gz
	"

prepare() {
	default_prepare

	rmdir thirdparty/geomag
	mv "$srcdir/geomag-$_commit_geomag" thirdparty/geomag
}

build() {
	qmake DEFAULT_BASEMAP=OpenCycleMap DEFAULT_ROUTER=OSRM FLAVOR=kirigami PREFIX=/usr
	make
}

check() {
	# Disabled as they require API keys to be present and/or an internet connection
	pytest geocoders guides poor routers \
		-k 'not test_geocode and not test_autocomplete_type and not test_nearby and not test_get and not test_autocomplete'
}

package() {
	INSTALL_ROOT="$pkgdir" make install

	# Locales get installed to the wrong location and thus have to be moved
	# to get picked up by abuild lang()
	mv "$pkgdir"/usr/share/pure-maps/locale "$pkgdir"/usr/share
}

lang() {
	default_lang

	amove usr/share/pure-maps/translations
}

sha512sums="1c23992efd93c2e61359af3ad28e235e898aa6d1a8bb51f10c07d927e70f839958396ed0614c4994673a943b34a1952fd838b95a83004799c04ce9415f9a0aa5  pure-maps-2.6.5.tar.gz
13e11b6cb35162315deb86c6c6240a3555760397d7aa88ac9c3348d476e9e9547b03210134119c60790511489e3f2a13afb93a3c77d40b1258c664b6fcc0425c  geomag-8eb9a730c8643fb7d63fdee4fd9a195ee8ba4df2.tar.gz"
